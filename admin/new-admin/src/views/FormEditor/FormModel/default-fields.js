export default {
  fields: 
    [
      {
        "id": 0,
        "type": "area",
        "title": "Título",
        "order": 0,
        "name": "default_area",
        "description": "Descrição",
        "content": "Escreva aqui"
      },
      {
        "id": 0,
        "type": "text",
        "title": "Título",
        "order": 0,
        "name": "default_text",
        "description": "Descrição",
        "content": "Escreva aqui"
      },
      {
        "id": 0,
        "type": "nps",
        "title": "Título",
        "order": 0,
        "name": "default_nps",
        "description": "Descrição"
      },
      {
        "id": 0,
        "type": "rating",
        "title": "Título",
        "order": 0,
        "name": "default_rating",
        "description": "Descrição"
      },
      {
        "id": 0,
        "type": "radio",
        "title": "Título",
        "order": 0,
        "name": "default_radio",
        "values": [
          "Valor 1",
          "Valor 2",
          "Valor 3"
        ],
        "description": "Descrição"
      },
      {
        "id": 0,
        "type": "checkbox",
        "title": "Título",
        "order": 0,
        "name": "default_checkbox",
        "values": [
          "Valor 1",
          "Valor 2",
          "Valor 3"
        ],
        "description": "Descrição"
      },
      {
        "id": 0,
        "type": "select",
        "title": "Título",
        "order": 0,
        "name": "default_select",
        "values": [
          "Valor 1",
          "Valor 2",
          "Valor 3"
        ],
        "description": "Descrição"
      }
    ]
};
