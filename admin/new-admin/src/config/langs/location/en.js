export default {
  lang: {
    app: {
      error: "Sorry, something get wrong:",
      no_connection: "Hey, looks like you have no internet connection available, try again in"
    },
    menu: {
      home: "Home",
      form_editor: "Customize forms"
    },
    home: {
      welcome: "Welcome to Yourviews",
      text: "Our purpose is to bring to your e-commerce a relationship of proximity with the final consumer and with that promote an environment that inspires confidence and credibility."
    },
    form_editor: {
      title: "Create awesome forms",
      forms_title: "Choose a form to edit",
      new_form: "New",
      save_btn: "Save",
      edit_form_info: "Name and description",
      back_btn: "Back",
      editing_form_title: "Name",
      editing_form_description: "Description",
      custom_field_name: "Name",
      default_field: {
        title_nps: "NPS",
        title_rating: "Star rating",
        title_area: "Multiple line text",
        title_text: "Single line text",
        title_radio: "Single selection",
        title_checkbox: "Multiple selection",
        title_select: "Single selection box"
      },
      field_edit: {
        main: {
          title: "Title",
          description: "Description",
          content: "Content",
          category: "Category",
          category_placeholder: "Choose a category",
          category_product: "Product",
          category_user: "User",
          category_store: "Store",
          required: "Required",
          hide: "Esconder ao exibir review"
        }
      }
    }
  }
};
