export default {
  lang: {
    app: {
      error: "Desculpe, mas tivemos um erro:",
      no_connection: "Ei, parece que você está sem conexão com a internet, tentando novamente em"
    },
    menu: {
      home: "Home",
      form_editor: "Editar formulários"
    },
    home: {
      welcome: "Bem vindo a Yourviews",
      text: "Nosso propósito é trazer para seu e-commerce uma relação de proximidade com o consumidor final e com isso promover um ambiente que inspire confiança e credibilidade."
    },
    form_editor: {
      title: "Crie formulários incríveis",
      forms_title: "Escolha um formulário para editar",
      new_form: "Novo",
      save_btn: "Salvar",
      edit_form_info: "Nome e descrição",
      back_btn: "Voltar",
      editing_form_title: "Nome",
      editing_form_description: "Descrição",
      custom_field_name: "Nome",
      default_field: {
        title_nps: "NPS",
        title_rating: "Avaliação com estrelas",
        title_area: "Texto de várias linhas",
        title_text: "Texto de uma linha",
        title_radio: "Seleção única",
        title_checkbox: "Seleção multipla",
        title_select: "Caixa de seleção única"
      },
      field_edit: {
        main: {
          title: "Título",
          description: "Descrição",
          content: "Conteúdo",
          category: "Categoria",
          category_placeholder: "Escolha uma categoria",
          category_product: "Produto",
          category_user: "Usuário",
          category_store: "Loja",
          required: "Obrigatório",
          hide: "Esconder ao exibir review"
        }
      }
    }
  }
};
