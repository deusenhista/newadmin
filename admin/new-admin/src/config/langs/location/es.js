export default {
  lang: {
    app: {
      error: "Lo sentimos, pero tuvimos un error:",
      no_connection: "Oye, parece que no tienes conexión a internet disponible, intentando nuevamente en"
    },
    menu: {
      home: "Home",
      form_editor: "Personalizar formularios"
    },
    home: {
      welcome: "¡ Bienvenido a Yourviews",
      text: "Nuestro propósito es traer a su comercio electrónico una relación de proximidad con el consumidor final y con ello promover un ambiente que inspire confianza y credibilidad."
    },
    form_editor: {
      title: "Crea formularios increíbles",
      forms_title: "Elija un formulario para editar",
      new_form: "Nuevo",
      save_btn: "Guardar",
      edit_form_info: "Nombre y descripción",
      back_btn: "Regreso",
      editing_form_title: "Nombre",
      editing_form_description: "Descripción",
      custom_field_name: "Nombre",
      default_field: {
        title_nps: "NPS",
        title_rating: "Calificación de estrellas",
        title_area: "Texto de varias líneas",
        title_text: "Texto de una sola línea",
        title_radio: "Selección única",
        title_checkbox: "Selección múltiple",
        title_select: "Cuadro de selección individual"
      },
      field_edit: {
        main: {
          title: "Título",
          description: "Descripción",
          content: "Contenido",
          category: "Categoría",
          category_placeholder: "Seleccione una categoría",
          category_product: "Producto",
          category_user: "Usuario",
          category_store: "Tienda",
          required: "Obligatorio",
          hide: "Esconder ao exibir review"
        }
      }
    }
  }
};
