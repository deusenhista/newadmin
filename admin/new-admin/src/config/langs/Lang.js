import english from "./location/en";
import portugues from "./location/pt";
import espanol from "./location/es";

export default {
  en: english.lang,
  pt: portugues.lang,
  es: espanol.lang,
};
