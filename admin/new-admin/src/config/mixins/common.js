export const common = {
  methods: {
    limitCharacters(text, chars = 120) {
      if(text.length > chars){
        text = text.substring(0,chars) + " ..."
      }
      return text
    }
  }
}