import Vue from 'vue'
import { MLInstaller, MLCreate, MLanguage } from 'vue-multilanguage'
import langs from "./langs/Lang";

Vue.use(MLInstaller)

export default new MLCreate({
  initial: 'Português',
  save: process.env.NODE_ENV === 'production',
  languages: [
    new MLanguage('Português').create(langs.pt),
    new MLanguage('English').create(langs.en),
    new MLanguage('Español').create(langs.es)
  ]
})