import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home/Home.vue";
import FormEditor from "./views/FormEditor/FormEditor.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/form-editor",
      name: "form-editor",
      component: FormEditor
    }
  ]
});
